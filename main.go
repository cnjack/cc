package main

import (
	"fmt"
	"net/http"
	"os"
	"os/exec"
)

func main() {
	port := os.Args[1]
	web(port)
}
func web(port string) {
	http.HandleFunc("/run", func(w http.ResponseWriter, r *http.Request) {
		cmd := run("ls")
		buf, _ := cmd.Output()
		fmt.Fprintf(w, string(buf))
	})
	fmt.Println("port " + port + " is listening")
	http.ListenAndServe(":"+port, nil)
}
func run(q string) *exec.Cmd {
	return exec.Command(q)
}
